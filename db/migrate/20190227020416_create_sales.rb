class CreateSales < ActiveRecord::Migration[5.2]
  def change
    create_table :sales do |t|
      t.string :description, null: false, default: ''
      t.float :total_price, null: false, default: 0.0

      t.timestamps
    end
  end
end
