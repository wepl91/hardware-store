class ChangeProductNameToTextNotString < ActiveRecord::Migration[5.2]
  def change
    change_column :products, :name, :text
  end
end
