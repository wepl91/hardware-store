class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to root_url, notice: "Haz iniciado sesión!!"
    else
      flash.now[:error] = "Alguno de los datos ingresados es incorrecto, intentalo nuevamente!"
      render "new"
    end
  end
  
  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: "Hasta la próxima!!"
  end
end